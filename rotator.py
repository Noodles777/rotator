class Rotator:
    __allowed_chars = [9,10,13]

    '''
    Rotate 'string' by 'places' number of places. By default
    this rotation ignores any non-alphabet characters, and keeps
    the result in the same rang as the input. This can be overridden
    by setting 'ignore_condition'.
    '''
    def rotate(self, string, places, ignore_condition = False):
        return_string = ""

        for c in string:
            ascii_val = ord(c)

            if(not ignore_condition and ascii_val < 32 and ascii_val not in self.__allowed_chars):
                continue

            new_val = ascii_val + places

            if(not ignore_condition):
                if(ascii_val > 64 and ascii_val < 91):
                    if(new_val > 90):
                        new_val = (new_val % 91) + 65
                elif(ascii_val > 96 and ascii_val < 123):
                    if(new_val > 122):
                        new_val = (new_val % 123) + 97
                else:
                    new_val = ascii_val

            return_string += chr(new_val)

        return return_string



