# README #

## INSTALLATION
1. Clone the repository to a directory of your choosing using:

    ` git clone git@bitbucket.org:Noodles777/rotator.git mydir`


### What is in this repository? ###
This repository contains two elements:

1. rotator.py

    This is a small python module, which defines the Rotator class. The Rotator class can be used to rotate a string by an arbitrary number
    of places.

2. rot13

     This is a small command line tool that makes use of the rotator module to allow users to rotate strings.